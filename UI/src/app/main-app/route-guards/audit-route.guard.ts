import { roleChecker } from '@/shared/RoleChecker';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable()
export class AuditRouteGuard implements CanActivate {
    constructor(
        private router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(roleChecker("Auditor"))
        {
            return true;
        }    
        this.router.navigate(['/app']);
        return false;
    }


}
