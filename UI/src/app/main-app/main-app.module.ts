import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAppComponent } from './main-app.component';
import { MainRoutingModule } from './main-app.routing';
import { SharedModule } from '@/shared/shared.module';
import { AuditRouteGuard } from './route-guards/audit-route.guard';

@NgModule({
    imports : [
        CommonModule,
        MainRoutingModule,
        SharedModule
    ],
    declarations : [
        MainAppComponent
    ],
    providers : [AuditRouteGuard]
})
export class MainAppModule {
}