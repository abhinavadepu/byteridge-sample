import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MainAppComponent } from "./main-app.component";
import { AuditRouteGuard } from "./route-guards/audit-route.guard";
const routes: Routes = [
  {
    path: "",
    component: MainAppComponent,
    children: [
      {
        path: "",
        redirectTo: "/app/Dashboard",
        pathMatch: "full",
      },
      {
        path: "Dashboard",
        loadChildren: () =>
          import("./dashboard/dashboard.module").then(
            (module) => module.DashboardModule
          ),
      },
      {
        path: "Audit",
        loadChildren: () =>
          import("./audit/audit.module").then((module) => module.AuditModule),
        canActivate: [AuditRouteGuard]
      },
    ],
  },
  {
    path: "**",
    redirectTo: "login",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
