import { AuditService } from '@/_services/audit.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    templateUrl: 'audit-list.component.html'
})

export class AuditListComponent implements OnInit {
    constructor(private _auditService : AuditService) { }
    $auditList : Observable<any[]>
    ngOnInit() { 
        this.$auditList = this._auditService.getAudits();
    }
}