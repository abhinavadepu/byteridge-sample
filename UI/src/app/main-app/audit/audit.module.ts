import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuditListComponent } from "./audit-list/audit-list.component";
import { SharedModule } from "@/shared/shared.module";
import { AuditRoutingModule } from "./audit.routing";

@NgModule({
  declarations: [AuditListComponent],
  imports: [CommonModule, SharedModule, AuditRoutingModule],
})
export class AuditModule {}
