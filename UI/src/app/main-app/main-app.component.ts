import { roleChecker } from '@/shared/RoleChecker';
import { AuthenticationService } from '@/_services';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'main-app.component.html'
})

export class MainAppComponent implements OnInit {
    roleChecker = roleChecker;
    constructor(
        private _authenticationService : AuthenticationService,
        private _router : Router
    ) { }


    ngOnInit() {
         
    }

    logout() {
        this._authenticationService.logout();
        this._router.navigate(['/signin']);
    }

}