import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '@/shared/config'
@Injectable({
    providedIn: 'root'
})
export class AuditService {
    constructor(private http: HttpClient) { }

    public updateAudit(auditObject) {
        return this.http.post(`${config.apiUrl}/audit/updateAuditSessionEnd`,{
            sessionEndTime:new Date().toUTCString(), 
            auditId : auditObject.auditId
        }).toPromise();
    }


    public getAudits() : Observable<any[]> { 
        return this.http.get<any[]>(`${config.apiUrl}/audit/getAudits`);
    }
}