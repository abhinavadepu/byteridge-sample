﻿import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';

const routes: Routes = [
    { path: '', redirectTo : 'app', pathMatch : "full", canActivate: [AuthGuard]  },
    { path: 'app', loadChildren : ()=>import('./main-app/main-app.module').then(module => module.MainAppModule) , canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo : 'login', pathMatch : "full" }
];

export const appRoutingModule = RouterModule.forRoot(routes);