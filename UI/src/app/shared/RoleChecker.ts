export var roleChecker = (role : string)=> {
    let userObject = JSON.parse(localStorage.getItem('currentUser')) || {};
    return (userObject.role || '').split(",").map(ele => ele.toLowerCase()).includes(role.toLowerCase());
}