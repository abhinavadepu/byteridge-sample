﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Dtos;
using WebApi.Entities;
using WebApi.Services.Audit;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AuditController : ControllerBase
    {
        private IAuditService _auditService;

        public AuditController(IAuditService audit)
        {
            _auditService = audit;
        }


        [Route("updateAuditSessionEnd")]
        [HttpPost]
        public async Task<IActionResult> UpdateAuditSessionEnd(AuditDto audit)
        {
            return Ok(await Task.FromResult(_auditService.UpdateAuditSessionEnd(audit.AuditId, audit.SessionEndTime)));
        }


        [Authorize(Roles = "Auditor")]
        [HttpGet("getAudits")]
        public async Task<IActionResult> GetAuditsAsync()
        {
            return Ok(await Task.FromResult(_auditService.GetAudits()));
        }
    }
}
