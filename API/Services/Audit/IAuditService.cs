﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;

namespace WebApi.Services.Audit
{
    public interface IAuditService
    {
        Guid AddAudit(Guid UserId, DateTime? SessionStartTime, DateTime? SessionEndTime, string NetworkIP);
        List<AuditResultView> GetAudits();
        bool UpdateAuditSessionEnd(Guid auditId, DateTime sessionEndTime);
    }
}
