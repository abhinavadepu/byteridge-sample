﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using WebApi.Entities;
using WebApi.Helpers;
using AuditClass = WebApi.Entities.Audit;

namespace WebApi.Services.Audit
{
    public class AuditService : IAuditService
    {
        private DataContext _context;
        public AuditService(DataContext dataContext)
        {
            _context = dataContext;
        }

        public Guid AddAudit(Guid userId, DateTime? sessionStartTime, DateTime? sessionEndTime, string networkIP)
        {
            Guid x = Guid.NewGuid();
            _context.Add(new AuditClass()
            {
                Id = x,
                NetworkIP = networkIP,
                SessionEndTime = sessionEndTime,
                SessionStartTime = sessionStartTime,
                UserId = userId
            });
            _context.SaveChanges();
            return x;
        }

        public List<AuditResultView> GetAudits()
        {
            return _context.Audits.Select(result => new AuditResultView()
            {
                FirstName = result.User.FirstName,
                LastName = result.User.LastName,
                PublicIP = result.NetworkIP,
                SessionEndTime = result.SessionEndTime,
                SessionStartTime = result.SessionStartTime
            }).ToList();
        }

        public bool UpdateAuditSessionEnd(Guid auditId, DateTime sessionEndTime)
        {
            _context.Audits.FirstOrDefault(ele => ele.Id == auditId).SessionEndTime = sessionEndTime;
            _context.SaveChanges();
            return true;
        }
    }
}
