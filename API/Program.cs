﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi
{
    public class Program
    {
        public Program()
        {
        }
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetService<DataContext>();
                byte[] passwordHash, passwordSalt;
                UserService.CreatePasswordHash("password", out passwordHash, out passwordSalt);
                context.Users.Add(new User()
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Auditor",
                    LastName = "Auditor",
                    Username = "auditor",
                    Role = "Auditor",
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt
                });
                context.SaveChanges();
            }
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://localhost:4000")
                .Build();
    }
}
